﻿module GameInterface

open System
open System.Runtime.InteropServices
open System.Diagnostics

// Rows & Cursors are 0 indexed
// Player confirms are 0 for not selected and 1 for selected
// Match winner is 1 for p1, 2 for p2, 0 when no winner
// Match pause menu is 0 when not open, 1 when open
// Health is the int number, 0 = no health, 3 bars = 40k
// Match pause exit confirm is 0 when not open, 2 for no, 1 for yes
// Char pick state, 0 is unpicked, 1 is transform select, 2 is custom select
//   3 is potara ring select, 5 is color select, 7 is character picked
//
// GameState is 0 for fight conclusion, 8 for loading screen,
//    14 for fight in progress, 80 for character/stage select
type Address =
    | Char1Row              = 0x20C4665C
    | Char1Cursor           = 0x20C46658
    | Char1TransformCursor  = 0x20C46660
    | Char1CustomCursor     = 0x20C4666C
    | Char1ColorCursor      = 0x20C46670
    | Char1PickState        = 0x20C466C8
    | Char2Row              = 0x20C4674C
    | Char2Cursor           = 0x20C46748
    | Char2TransformCursor  = 0x20C46750
    | Char2CustomCursor     = 0x20C4675C
    | Char2ColorCursor      = 0x20C46760
    | Char2PickState        = 0x20C467B8
    | Player1CharConfirm    = 0x20C49B40
    | Player2CharConfirm    = 0x20C49B54
    | StageCursor           = 0x20C46840
    | StageRow              = 0x20C46844
    | Player1Health         = 0x218710A4
    | Player2Health         = 0x218726A4
    | MatchWinner           = 0x20333700
    | MatchPauseMenu        = 0x202C5420
    | MatchPauseExitConfirm = 0x202C4F18
    | GameState             = 0x207BDBF8
 


type Key =
    | WM_KEYDOWN        = 0x0100
    | WM_KEYUP          = 0x0101
    | CROSS             = 0x5A
    | CIRCLE            = 0x58
    | SQUARE            = 0x41
    | TRIANGLE          = 0x53
    | LEFT              = 0x25
    | UP                = 0x26
    | RIGHT             = 0x27
    | DOWN              = 0x28


[<DllImport("kernel32.dll", CharSet = CharSet.Unicode)>]
extern bool ReadProcessMemory(int hProcess,
        int lpBaseAddress, byte[] lpBuffer, int dwSize, int& lpNumberOfBytesRead)

[<DllImport("kernel32.dll")>]
extern IntPtr OpenProcess(int dwDesiredAccess, bool bInheritHandle, int dwProcessId)
        
[<DllImport("user32.dll")>]
extern IntPtr SetForegroundWindow(IntPtr hWnd)

[<DllImport("user32.dll")>]
extern int PostMessage(IntPtr hWnd, uint32 Msg, int wParam, int lParam)

[<DllImport("user32.dll", CharSet = CharSet.Unicode)>]
extern IntPtr FindWindow(string lpClassName, string lpWindowName)

[<DllImport("user32.dll", CharSet = CharSet.Unicode)>]
extern IntPtr SetFocus(IntPtr hWnd)

[<DllImport("user32.dll", CharSet = CharSet.Unicode)>]
extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string className, string windowTitle)

[<DllImport("user32.dll", CharSet = CharSet.Unicode)>]
extern uint32 MapVirtualKey(uint32 uCode, uint32 uMapType);

let getHandle name className =
    let gameProcess = 
        Process.GetProcessesByName(name)
        |> Array.head
    let PROCESS_VM_READ = 0x0010
    let gameHwnd = FindWindowEx(gameProcess.MainWindowHandle, IntPtr.Zero, className, null)
    (OpenProcess(PROCESS_VM_READ, false, gameProcess.Id), gameHwnd)

let readMemory (address: Address) (handle: IntPtr) =
    let mutable buffer = Array.init 4 byte
    let mutable value = 0
    ReadProcessMemory((int)handle, (int)address, buffer, buffer.Length, &value) |> ignore
    BitConverter.ToInt32(buffer, 0)

let executeCommand (key: Key) (handle: IntPtr) =
    async {
        let lParam = (MapVirtualKey(uint32(key), (uint32)0) <<< 16) + (uint32)1
    
        PostMessage(handle, (uint32)Key.WM_KEYDOWN, (int)key, (int)lParam) |> ignore
        do! Async.Sleep(50)
        PostMessage(handle, (uint32)Key.WM_KEYUP, (int)key, (int)lParam) |> ignore
    }
