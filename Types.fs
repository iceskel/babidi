﻿module Types

open Microsoft.Extensions.DependencyInjection
open System.Collections


type Character = 
    Goku
    | Vegeta
    | Gohan

type Spell = {
    MatchId: string
    Player1: string
    Player2: string
    Stage: string
    Bestof: int
}

type ConjureSpell = Spell -> Spell

type ErrorMessage = {
    Message: string
}

type Errored = ErrorMessage -> ErrorMessage


type Event =
    | CharacterSelect
    | StageSelect
    | Fight

type Units = {
    Row: int;
    Column: int;
}

type Pick = {
    BaseForm: Units;
    TransformCursor: int option;
}

type Characters = {
    P1: Pick;
    P2: Pick;
}

type Target = {
    Characters: Characters option;
    Stage: Units option;
}

type GameState =
    | FightEnd
    | Loading
    | Fighting 
    | CharacterPick
    | Unknown

type Message = {
    event: Event;
    target: Target;
}

let getCharacter (player: string) =
    match player.ToLower() with
    | "goku (early)"        -> Some { BaseForm = { Row = 0; Column = 0}; TransformCursor = None }
    | "kid gohan"           -> Some { BaseForm = { Row = 0; Column = 1}; TransformCursor = None }
    | "piccolo (early)"     -> Some { BaseForm = { Row = 0; Column = 2}; TransformCursor = None }
    | "vegeta (scouter)"    -> Some { BaseForm = { Row = 0; Column = 3}; TransformCursor = Some 0 }
    | "vegeta (ape)"        -> Some { BaseForm = { Row = 0; Column = 3}; TransformCursor = Some 1 }
    | "krillin"             -> Some { BaseForm = { Row = 0; Column = 4}; TransformCursor = None }
    | "yamcha"              -> Some { BaseForm = { Row = 0; Column = 5}; TransformCursor = None }
    | "trunks (sword)"      -> Some { BaseForm = { Row = 0; Column = 6}; TransformCursor = Some 0 }
    | "ssj trunks (sword)"  -> Some { BaseForm = { Row = 0; Column = 6}; TransformCursor = Some 1 }
    | "goku (mid)"          -> Some { BaseForm = { Row = 1; Column = 0}; TransformCursor = Some 0 }
    | "ssj goku (mid)"      -> Some { BaseForm = { Row = 1; Column = 0}; TransformCursor = Some 1 }
    | "teen gohan"          -> Some { BaseForm = { Row = 1; Column = 1}; TransformCursor = Some 0 }
    | "ssj teen gohan"      -> Some { BaseForm = { Row = 1; Column = 1}; TransformCursor = Some 1 }
    | "ssj2 teen gohan"     -> Some { BaseForm = { Row = 1; Column = 1}; TransformCursor = Some 2 }
    | "piccolo (end)"       -> Some { BaseForm = { Row = 1; Column = 2}; TransformCursor = None }
    | "vegeta"              -> Some { BaseForm = { Row = 1; Column = 3}; TransformCursor = Some 0 }
    | "ssj vegeta"          -> Some { BaseForm = { Row = 1; Column = 3}; TransformCursor = Some 1 }
    | "super vegeta"        -> Some { BaseForm = { Row = 1; Column = 3}; TransformCursor = Some 2 }
    | "tien"                -> Some { BaseForm = { Row = 1; Column = 4}; TransformCursor = None }
    | "chiaotzu"            -> Some { BaseForm = { Row = 1; Column = 5}; TransformCursor = None }
    | "trunks"              -> Some { BaseForm = { Row = 1; Column = 6}; TransformCursor = Some 0 }
    | "ssj trunks"          -> Some { BaseForm = { Row = 1; Column = 6}; TransformCursor = Some 1 }
    | "super trunks"        -> Some { BaseForm = { Row = 1; Column = 6}; TransformCursor = Some 2 }
    | _                     -> None

let getStage (stage: string) =
    match stage.ToLower() with
    | "random"      -> Some { Row = 5; Column = 5 }
    | "cell games"  -> Some { Row = 2; Column = 4 }
    | _             -> None

let save (inMemory : Hashtable) (spell : Spell) : Spell =
  inMemory.Add(spell.MatchId, spell) |> ignore
  spell

type IServiceCollection with
  member this.AddSpellInMemory (inMemory : Hashtable) =
    this.AddSingleton<ConjureSpell>(save inMemory) |> ignore