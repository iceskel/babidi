﻿module Enchant

open GameInterface
open Types


let currentGameState handle =
    match readMemory Address.GameState handle with
    | 0     -> GameState.FightEnd
    | 8     -> GameState.Loading
    | 14    -> GameState.Fighting
    | 80    -> GameState.CharacterPick
    | _     -> GameState.Unknown

let currentCharacterValue handle = 
    {
        P1 = { BaseForm = { Row = readMemory Address.Char1Row handle;
                            Column = readMemory Address.Char1Cursor handle};
                            TransformCursor = Some (readMemory Address.Char1TransformCursor handle)};
        P2 = { BaseForm = { Row = readMemory Address.Char2Row handle;
                    Column = readMemory Address.Char2Cursor handle};
                    TransformCursor = Some (readMemory Address.Char2TransformCursor handle)};
    }

let chant handle = MailboxProcessor.Start(fun inbox ->
    let rec pickingCharacters(msg: Message) = async {
        match msg.event with
        | StageSelect ->
            let pickState = (readMemory Address.Player1CharConfirm (fst handle), readMemory Address.Player2CharConfirm (fst handle))
            match pickState with
            | (1, 1) ->
                return! pickingStage(msg)
            | _ ->
                let! newMsg = inbox.Receive()
                return! pickingCharacters(newMsg)
            
        | Fight -> return! pickingCharacters(msg)
        | CharacterSelect ->
            let rec characterLoop (pos: Pick) (target: Pick) (f: unit -> Pick) =
                match pos with
                | pos when pos.BaseForm.Row < target.BaseForm.Row ->
                    async {
                        do! executeCommand Key.DOWN (snd handle)
                        do! Async.Sleep(50)
                        return! characterLoop (f ()) target f
                    }
                | pos when pos.BaseForm.Row > target.BaseForm.Row ->
                    async {
                        do! executeCommand Key.UP (snd handle)
                        do! Async.Sleep(50)
                        return! characterLoop (f ()) target f
                    }
                | pos when pos.BaseForm.Column < target.BaseForm.Column ->
                    async {
                        do! executeCommand Key.RIGHT (snd handle)
                        do! Async.Sleep(50)
                        return! characterLoop (f ()) target f
                    }
                | pos when pos.BaseForm.Column > target.BaseForm.Column ->
                       async {
                           do! executeCommand Key.LEFT (snd handle)
                           do! Async.Sleep(50)
                           return! characterLoop (f ()) target f
                       }
                | pos -> 
                    async {
                        do! Async.Sleep(50)
                        do! executeCommand Key.CROSS (snd handle)
                        do! Async.Sleep(50)
                        let rec transformLoop (pos: Pick) =
                            match pos.TransformCursor with
                                    | Some cursor when cursor < target.TransformCursor.Value ->
                                        async {
                                            do! executeCommand Key.RIGHT (snd handle)
                                            do! Async.Sleep(50)
                                            return! transformLoop (f ())
                                        }
                                    | Some cursor when cursor > target.TransformCursor.Value ->
                                        async {
                                            do! executeCommand Key.LEFT (snd handle)
                                            do! Async.Sleep(50)
                                            return! transformLoop (f ())
                                        }
                                    | Some _ ->
                                        async {
                                            do! Async.Sleep(50)
                                            do! executeCommand Key.CROSS (snd handle)
                                            do! Async.Sleep(50)
                                            return () 
                                        }
                                    | None -> async { return () }
                        if (target.TransformCursor.IsNone)
                        then return ()
                        else return! transformLoop (f ())
                    }
                   
                | _ ->
                    async {
                        return ()
                    }
            let rec customLoop (cursor: int) (f: System.IntPtr -> int) =
                match cursor with
                | cursor when cursor < 1 ->
                    async {
                        do! Async.Sleep(50)
                        do! executeCommand Key.DOWN (snd handle)
                        do! Async.Sleep(50)
                        return! customLoop (f (fst handle)) f
                    }
                | cursor when cursor > 1 ->
                    async {
                        do! Async.Sleep(50)
                        do! executeCommand Key.UP (snd handle)
                        do! Async.Sleep(50)
                        return! customLoop (f (fst handle)) f
                    }
                | _ ->
                    async {
                        do! Async.Sleep(50)
                        do! executeCommand Key.CROSS (snd handle)
                        do! Async.Sleep(50)
                        return ()
                    }
            
            let rec colorLoop  =
                async {
                    do! Async.Sleep(50)
                    do! executeCommand Key.CROSS (snd handle)
                    do! Async.Sleep(50)
                }
            
            let pickState = (readMemory Address.Player1CharConfirm (fst handle), readMemory Address.Player2CharConfirm (fst handle))
            match pickState with
            | (1, 1) ->
                let! newMsg = inbox.Receive()
                return! pickingCharacters(newMsg)
            | (1, _) ->
                do! characterLoop (currentCharacterValue (fst handle)).P2 msg.target.Characters.Value.P2 (fun i -> (currentCharacterValue (fst handle)).P2)
                let p2CustomCursor = (readMemory Address.Char2CustomCursor)
                do! customLoop (p2CustomCursor (fst handle)) p2CustomCursor
                do! colorLoop

                let! newMsg = inbox.Receive()
                return! pickingCharacters(newMsg)
            | (_, 1) ->
                do! characterLoop (currentCharacterValue (fst handle)).P1 msg.target.Characters.Value.P1 (fun i -> (currentCharacterValue (fst handle)).P1)
                let p1CustomCursor = (readMemory Address.Char1CustomCursor)
                do! customLoop (p1CustomCursor (fst handle)) p1CustomCursor
                do! colorLoop

                let! newMsg = inbox.Receive()
                return! pickingCharacters(newMsg)
            | _ ->
                do! characterLoop (currentCharacterValue (fst handle)).P1 msg.target.Characters.Value.P1 (fun i -> (currentCharacterValue (fst handle)).P1)
                let p1CustomCursor = (readMemory Address.Char1CustomCursor)
                do! customLoop (p1CustomCursor (fst handle)) p1CustomCursor
                do! colorLoop

                do! Async.Sleep(100)

                do! characterLoop (currentCharacterValue (fst handle)).P2 msg.target.Characters.Value.P2 (fun i -> (currentCharacterValue (fst handle)).P2)
                let p2CustomCursor = (readMemory Address.Char2CustomCursor)
                do! customLoop (p2CustomCursor (fst handle)) p2CustomCursor
                do! colorLoop

                let! newMsg = inbox.Receive()
                return! pickingCharacters(newMsg)
        | _ -> return ()}
      

    and pickingStage(msg: Message) = async {
        match msg.event with 
        | CharacterSelect -> return ()
        | StageSelect ->
            let rec stageLoop (pos: Units) (target: Units) (f: unit -> Units) =
                match pos with
                | pos when pos.Row < target.Row ->
                    async {
                        do! Async.Sleep(50)
                        do! executeCommand Key.DOWN (snd handle)
                        do! Async.Sleep(50)
                        return! stageLoop (f ()) target f
                    }
                | pos when pos.Row > target.Row ->
                    async {
                        do! Async.Sleep(50)
                        do! executeCommand Key.UP (snd handle)
                        do! Async.Sleep(50)
                        return! stageLoop (f ()) target f
                    }
                | pos when pos.Column < target.Column ->
                    async {
                        do! Async.Sleep(50)
                        do! executeCommand Key.RIGHT (snd handle)
                        do! Async.Sleep(50)
                        return! stageLoop (f ()) target f
                    }
                | pos when pos.Column > target.Column ->
                    async {
                        do! Async.Sleep(50)
                        do! executeCommand Key.LEFT (snd handle)
                        do! Async.Sleep(50)
                        return! stageLoop (f ()) target f
                    }
                | _ ->
                    async {
                        do! Async.Sleep(100)
                        do! executeCommand Key.CROSS (snd handle)
                        do! Async.Sleep(50)
                        return ()
                    }

            let currentStageValue handle = 
                {
                    Row = readMemory Address.StageRow handle;
                    Column = readMemory Address.StageCursor handle;
                }

            do! stageLoop (currentStageValue (fst handle)) msg.target.Stage.Value (fun i -> (currentStageValue (fst handle)))
            let! newEvent = inbox.Receive()
            return! pickingStage(newEvent)
        | _ -> return ()}

    and fightingState(msg: Message) = async {
        match msg.event with
        | CharacterSelect -> return! pickingCharacters(msg)
        | _ ->
            let rec fightingLoop (state: GameState) =
                match state with
                | GameState.Fighting ->
                    async {
                        return ()
                    }
                | GameState.FightEnd ->
                     async {
                        return ()
                    }
                | _ ->
                    async {
                        return ()
                    }

            do! fightingLoop (currentGameState (fst handle))
            return! async {return ()}}
    

    // init state
    async {
        let! msg = inbox.Receive()
        do! pickingCharacters(msg)
    }
)

module Singleton =
    let private x = Lazy.Create(fun() -> chant (getHandle "pcsx2" "wxWindowNR"))
    let GetInstance() = x.Value