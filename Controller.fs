﻿module Controller

open Giraffe
open Microsoft.AspNetCore.Http
open FSharp.Control.Tasks.V2
open Types
open System
open GameInterface


let handlers: HttpFunc -> HttpContext -> HttpFuncResult =
    choose [
        POST >=> route "/stage" >=>
            fun next context ->
                task {
                    let! spell = context.BindJsonAsync<Spell>()
                    let handle = getHandle "pcsx2" "wxWindowNR"
                    SetForegroundWindow(snd handle) |> ignore
                    do! Async.Sleep(500)
                    let stage = getStage spell.Stage

                    if stage.IsNone
                    then failwith "Stage not found"

                    let dat = {
                        Characters = None
                        Stage = stage
                    }

                    let getMessage event =
                        {
                            event = event;
                            target = dat;
                        }
                    Enchant.Singleton.GetInstance().Post (getMessage StageSelect)
                    return! next context
                }
        POST >=> route "/characters" >=>
            fun next context ->
                task {
                    let save = context.GetService<ConjureSpell>()
                    let! spell = context.BindJsonAsync<Spell>()
                    let handle = getHandle "pcsx2" "wxWindowNR"
                    SetForegroundWindow(snd handle) |> ignore
                    do! Async.Sleep(500)
                    let p1 = getCharacter spell.Player1
                    let p2 = getCharacter spell.Player2



                    if p1.IsNone
                    then failwith "Character not found"

                    if p2.IsNone
                    then failwith "Character not found"

                    let dat = {
                        Characters = Some {
                            P1 = p1.Value;
                            P2 = p2.Value;
                        };
                        Stage = None
                    }

                    //let song = Enchant.chant handle
                    //song.Post CharacterSelect
                    //song.Post StageSelect
                    //song.Post Fight

                    let getMessage event =
                        {
                            event = event;
                            target = dat;
                        }
                    
                    Enchant.Singleton.GetInstance().Post (getMessage CharacterSelect)
                    //Enchant.Singleton.GetInstance().Post (getMessage StageSelect)



                    //let character1value = readMemory Address.Char1Row (fst handle)
                    //executeCommand Key.DOWN (new IntPtr(0x00505F2))
                    //do! executeCommand Key.CROSS (snd handle)

                    let spell = { spell with MatchId = ShortGuid.fromGuid(Guid.NewGuid()) }
                    return! json (save spell) next context
                }
        GET >=> route "/conjure" >=>
            fun next context ->
                text "Read" next context
    ] 